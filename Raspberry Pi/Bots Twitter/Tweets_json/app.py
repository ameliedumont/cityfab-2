from flask import *
from time import sleep
import json
from flask_weasyprint import HTML, render_pdf

app = Flask(__name__)

with open('tweets.json') as json_file:
    data = json.load(json_file)
    liste = list(data)
    rliste = reversed(list(data))

@app.route("/")
def index():
    return render_template('index.html', value=data, list=liste, rlist=rliste)

@app.route('/hello.pdf')
def hello_pdf():
    html = render_template('index.html', value=data, list=liste, rlist=rliste)
    return render_pdf(HTML(string=html))

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')
