# -*- coding: utf-8 -*-

import tweepy
import requests
import threading
import json
from time import sleep
import os.path
import sys


consumer_key='****'
consumer_secret='****'
access_token='****'
access_token_secret='****'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

continue_loops = True


query = 'raspberry pi'

tweet_dict = {}
# charger du disque le json / déjsonner
if os.path.isfile('tweets.json'):
    with open("tweets.json", "r") as read_file:
    	data = json.load(read_file)
    	print( 'json > python',data )
    	read_file.close()


#fails = 0

def get_my_tweets():
    global fails
    while continue_loops == True:
        tweets = api.search(q=query, lang="en", count=1, tweet_mode="extended", wait_on_rate_limit = True)
        smthnew = False
        for tweet in tweets:
            if not tweet._json[u'id'] in tweet_dict:
                tweet_dict[ tweet._json[u'id'] ] = {
                'content': tweet.full_text,
                'timestamp': str(tweet.created_at)
                #'try_number': fails
                }

                smthnew = True
        if smthnew:
            fails = 0;
            jdata = json.dumps( tweet_dict )
            print('python > json',jdata)

            with open("tweets.json", "w") as write_file:
	            json.dump( tweet_dict, write_file )
	            write_file.close()

        sleep(1)


"""
def intervalle_tweets():
    while continue_loops == True:
        global fails
        fails += 1
        sleep(1)
"""

thread_tweets = threading.Thread(target = get_my_tweets)
thread_tweets.daemon = True
thread_tweets.start()

"""
thread_intervalle = threading.Thread(target = intervalle_tweets)
thread_intervalle.daemon = True
thread_intervalle.start()
"""

while continue_loops:
	sleep( 0.5 )
