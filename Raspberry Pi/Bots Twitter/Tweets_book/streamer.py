import tweepy as tp
import json

consumer_key = '****'
consumer_secret = '****'
access_token = '****'
access_secret = '****'

auth = tp.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
api = tp.API(auth)

data = []

class MyStreamListener(tp.StreamListener):
    def on_data(self, tweet):
        tweet_data = json.loads(tweet)
        data.append(tweet_data)

myStreamListener = MyStreamListener()
myStream = tp.Stream(auth = api.auth, listener=myStreamListener)

myStream.filter(track=['python'], is_async=True)
