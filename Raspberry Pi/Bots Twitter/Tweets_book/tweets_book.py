from flask import *
from flask_weasyprint import HTML, render_pdf
from time import sleep
from streamer import *

app = Flask(__name__)
app.jinja_options['extensions'].append('jinja2.ext.do')

@app.route("/")
def index():
    return render_template('index.html', len = len(data), value = data)

@app.route('/print')
def print():
    html = render_template('index.html', len = len(data), value = data)
    return render_pdf(HTML(string=html))

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')
