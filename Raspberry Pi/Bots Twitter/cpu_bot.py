#!/usr/bin/env python
import os
from twython import Twython

CONSUMER_KEY = '****'
CONSUMER_SECRET = '****'
ACCESS_KEY = '****'
ACCESS_SECRET = '****'

api = Twython(CONSUMER_KEY,CONSUMER_SECRET,ACCESS_KEY,ACCESS_SECRET)

cmd = '/opt/vc/bin/vcgencmd measure_temp'
line = os.popen(cmd).readline().strip()
temp = line.split('=')[1].split("'")[0]
api.update_status(status='My current CPU temperature is '+temp+' C')
