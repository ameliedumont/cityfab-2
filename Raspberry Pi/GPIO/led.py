import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

led = 4 

GPIO.setup(led,GPIO.OUT)

for i in range(5):
	print "LED on"
	GPIO.output(led,1)
	time.sleep(2)
	print "LED off"
	GPIO.output(led,0)
	time.sleep(2)
