#!/usr/bin/python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import random
from time import sleep

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)

pwm = GPIO.PWM(12, 50)
pwm.start(0)

def setAngle(angle):
    #envoyer commande au servo
    GPIO.output(12, True)
    #transformer un angle entre 0 et 180 en DutyCycle entre 2 et 12
    #Duty = angle/ 18 + 2
    pwm.ChangeDutyCycle(angle / 18 + 2)
    sleep(1)
    GPIO.output(12, False)
    pwm.ChangeDutyCycle(0)

value = random.randrange(1, 180)
setAngle(value)

pwm.stop()
GPIO.cleanup()
