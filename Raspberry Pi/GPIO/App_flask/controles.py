from flask import Flask, render_template, jsonify
from servo import *
import RPi.GPIO as GPIO
import random
from time import sleep

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(11, GPIO.IN)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
pwm = GPIO.PWM(12, 50)
pwm.start(0)

app = Flask(__name__)

last_valid_value = value
led_state = 0
piezo_state = 0
motor_dir = ""

@app.route('/')
def index():
	return render_template('index.html', value=value)

@app.route('/led_on/')
def led_on():
	global led_state
	led_state += 1
	GPIO.output(15, led_state)
	return jsonify(led_state)

@app.route('/led_off/')
def led_off():
	global led_state
	led_state -= 1
	GPIO.output(15, led_state)
	return jsonify(led_state)

@app.route('/motor_o/')
def motor_o():
	global motor_dir
	motor_dir = "onwards"
	GPIO.output(16, GPIO.HIGH)
	GPIO.output(18, GPIO.LOW)
	GPIO.output(22, GPIO.HIGH)
	sleep(2)
	GPIO.output(22, GPIO.LOW)
	return jsonify(motor_dir)

@app.route('/motor_b/')
def motor_b():
	global motor_dir
	motor_dir = "backwards"
	GPIO.output(16, GPIO.LOW)
	GPIO.output(18, GPIO.HIGH)
	GPIO.output(22, GPIO.HIGH)
	sleep(2)
	GPIO.output(22, GPIO.LOW)
	return jsonify(motor_dir)

@app.route('/down/')
def down():
	global last_valid_value
	last_valid_value -= 5
	while last_valid_value < 0:
		last_valid_value += 180
	setAngle(last_valid_value)
	return jsonify(last_valid_value)

@app.route('/up/')
def up():
	global last_valid_value
	last_valid_value += 5
	last_valid_value %= 180
	setAngle(last_valid_value)
	return jsonify(last_valid_value)

@app.route('/piezo/')
def piezo():
	GPIO.output(13, piezo_state)
	sleep(1)
	return jsonify(piezo_state)

@app.route('/get_value/')
def get_value():
	return jsonify(last_valid_value)

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')
