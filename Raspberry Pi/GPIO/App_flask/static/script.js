texte_led = document.getElementById("txt_led");

function led_on() {
  fetch("http://cityfab2.local:5000/led_on/");
  texte_led.innerHTML = "La DEL est allumée";
}

function led_off() {
  fetch("http://cityfab2.local:5000/led_off/");
  texte_led.innerHTML = "La DEL est éteinte";
}

function motor_o() {
  fetch("http://cityfab2.local:5000/motor_o/");
}

function motor_b() {
  fetch("http://cityfab2.local:5000/motor_b/");
}

function piezo() {
  fetch("http://cityfab2.local:5000/piezo/");
}

function up() {
  fetch("http://cityfab2.local:5000/up/");
}

function down() {
  fetch("http://cityfab2.local:5000/down/");
}

function display_value() {
  fetch('http://cityfab2.local:5000/get_value/')
  .then((resp) => resp.json()) // Transform the data into json
  .then(function(data) {
    //console.log( data );
    //console.log( JSON.stringify(data) );
    document.getElementById('servo_value').innerHTML = JSON.stringify(data);
  })
}
window.setInterval( display_value, 200 );
