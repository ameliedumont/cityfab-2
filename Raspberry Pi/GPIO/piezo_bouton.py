import RPi.GPIO as GPIO

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BOARD)

GPIO.setup(7,GPIO.OUT)
GPIO.setup(12, GPIO.IN)

while True:
	if GPIO.input(12):
		print('No beep')
		GPIO.output(7, GPIO.LOW)
	else:
		print('Beep')
		GPIO.output(7, GPIO.HIGH)
