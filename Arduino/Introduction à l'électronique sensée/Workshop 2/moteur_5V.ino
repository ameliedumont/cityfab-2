int pinMoteur=9;

void setup(){
    pinMode(pinMoteur,OUTPUT);
}

void loop(){
    digitalWrite(pinMoteur,LOW); //le moteur se lance
    delay(1000);
    digitalWrite(pinMoteur,HIGH); //le moteur s'arrête
    delay(1000);
}
