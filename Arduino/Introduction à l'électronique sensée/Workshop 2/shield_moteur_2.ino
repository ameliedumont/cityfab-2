/* 
This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
It won't work with v1.x motor shields! Only for the v2's with built in PWM
control

For use with the Adafruit Motor Shield v2 
---->	http://www.adafruit.com/products/1438
*/

#include <Wire.h>
#include <Adafruit_MotorShield.h>


// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMSbot = Adafruit_MotorShield(0x60); 
Adafruit_MotorShield AFMStop = Adafruit_MotorShield(0x61); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *myMotor1 = AFMSbot.getMotor(1);
Adafruit_DCMotor *myMotor2 = AFMSbot.getMotor(2);

Adafruit_StepperMotor *myStepper = AFMStop.getStepper(200, 2);


void setup() {
  Serial.begin(9600);  

  AFMSbot.begin();
  AFMStop.begin();// create with the default frequency 1.6KHz

  myMotor1->setSpeed(50);
  myMotor1->run(FORWARD);
  myMotor1->run(RELEASE);

  myMotor2->setSpeed(150);
  myMotor2->run(FORWARD);
  myMotor2->run(RELEASE);
}

void loop() {

  myMotor1->setSpeed(50);
  myMotor1->run(FORWARD);

  myMotor2->setSpeed(50);
  myMotor2->run(BACKWARD);

    Serial.println("Single coil steps");
  myStepper->step(100, FORWARD, SINGLE); 
  myStepper->step(100, BACKWARD, SINGLE); 

  Serial.println("Double coil steps");
  myStepper->step(100, FORWARD, DOUBLE); 
  myStepper->step(100, BACKWARD, DOUBLE);
  
  Serial.println("Interleave coil steps");
  myStepper->step(100, FORWARD, INTERLEAVE); 
  myStepper->step(100, BACKWARD, INTERLEAVE); 
  
  Serial.println("Microstep steps");
  myStepper->step(50, FORWARD, MICROSTEP); 
  myStepper->step(50, BACKWARD, MICROSTEP);

}
