/*
  Arduino Starter Kit example
  Project 11 - Crystal Ball

  This sketch is written to accompany Project 11 in the Arduino Starter Kit

  Parts required:
  - 220 ohm resistor
  - 10 kilohm resistor
  - 10 kilohm potentiometer
  - 16x2 LCD screen
  - tilt switch

  created 13 Sep 2012
  by Scott Fitzgerald

  http://www.arduino.cc/starterKit

  This example code is part of the public domain.
*/

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// set up a constant for the tilt switch pin
const int sensorPin = A0;

// variable to hold the value of the switch pin
int temperature = 0;

// variable to hold previous value of the switch pin
//int prevSwitchState = 0;

// a variable to choose which reply from the crystal ball
int reply;

void setup() {
  // set up the number of columns and rows on the LCD
  lcd.begin(16, 2);
  Serial.begin(9600);
  // set up the switch pin as an input
  pinMode(sensorPin, INPUT);

}

void loop() {
  // check the status of the switch
  temperature = analogRead(sensorPin);
  float voltage = (temperature/1024.0) * 5.0;
  float celsius = (voltage - .5) * 100;
  String phrase;
  phrase += "Il fait ";
  phrase += String(celsius);
  phrase += "\xB0";
  phrase += "C";
  lcd.print(phrase);
  delay(10);
  lcd.clear();
  Serial.println(phrase);
}
