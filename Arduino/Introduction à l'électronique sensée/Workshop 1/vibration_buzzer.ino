const int buzzer = 8; 
int sensor;           


void setup()
{
  Serial.begin(9600);      
  pinMode(buzzer, OUTPUT);
  pinMode(sensor, INPUT);
}

void loop()
{
  sensor = analogRead(A0);
  if (sensor > 600){
    tone(buzzer, sensor);
    Serial.print("Sensor Value: ");
    Serial.println(sensor);
  }
  
  else{ 
    noTone(buzzer);
    Serial.print("Sensor Value: ");
    Serial.println(sensor);
  }
  
  
  delay(100); //Small delay
}
