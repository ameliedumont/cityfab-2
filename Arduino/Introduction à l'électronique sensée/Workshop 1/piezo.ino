int potValue;
int sensorLow = 1023;
int sensorHigh = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  potValue = analogRead(A0);
  Serial.println(potValue);
  int pitch = map(potValue, sensorLow, sensorHigh, 50, 4000);
  tone(8, pitch, 20);
}
