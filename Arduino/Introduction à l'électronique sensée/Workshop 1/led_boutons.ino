int switchState1 = 0;
int switchState2 = 0;

void setup() {
  // On définit les modes des broches
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(12, OUTPUT);
}

void loop() {
  // On lit la position des boutons
  switchState1 = digitalRead(2);
  switchState2 = digitalRead(3);

  //Si le bouton 1 est pressé
  if(switchState1 == HIGH) {
    digitalWrite(12, HIGH);
    delay(1000);
    digitalWrite(12, LOW);
    delay(1000);
  } 
  //Si le bouton 2 est pressé
  else if(switchState2 == HIGH) {
    digitalWrite(12, HIGH);
    delay(250);
    digitalWrite(12, LOW);
    delay(250);
  }
  //Sinon on ne fait pas clignoter la LED
  else {
    digitalWrite(12, LOW);
  }
  

}
