import oscP5.*;
import netP5.*;
import processing.serial.*;

OscP5 oscP5;
NetAddress myRemoteLocation;
Serial myPort;
int lf = 10;
String myString = null;
float num;

void setup() {
  size(400, 400);
  frameRate(25);
  oscP5 = new OscP5(this, 12000);
  myRemoteLocation = new NetAddress("192.168.12.255", 12000);
  String portName = "/dev/ttyACM0";
  myPort = new Serial(this, Serial.list()[0], 9600);
  myPort.clear();
}


void draw() {
  background(0);
  if (myPort.available() > 0) {
    myString = myPort.readStringUntil(lf);
    if (myString != null) { 
      num = float(myString);
      if(num > 600) {
        OscMessage myMessage1 = new OscMessage("/synth");
        myMessage1.add(123); 
        oscP5.send(myMessage1, myRemoteLocation);
      } else {
        OscMessage myMessage2 = new OscMessage("/vide");
        myMessage2.add(0); 
        oscP5.send(myMessage2, myRemoteLocation);
      }
    }
  }

}

void mousePressed() {
  /* in the following different ways of creating osc messages are shown by example */
  OscMessage myMessage1 = new OscMessage("/synth");

  myMessage1.add(123); 
  oscP5.send(myMessage1, myRemoteLocation);
}


/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: "+theOscMessage.addrPattern());
  println(" typetag: "+theOscMessage.typetag());
  if (theOscMessage.checkTypetag("i")) {
    println(" typetag: "+theOscMessage.get(0).intValue());
  }
  if (theOscMessage.checkTypetag("ii")) {
    println(" First value : "+theOscMessage.get(0).intValue());
    println(" Second Value : "+theOscMessage.get(1).intValue());
  }
}
