import processing.serial.*;

int lf = 10;    // Linefeed in ASCII
String myString = null;
Serial myPort;  // Serial port you are using
float num;

void setup() {
  size(600, 400);
  myPort = new Serial(this, Serial.list()[0], 9600);
  myPort.clear();
  colorMode(RGB, 100);
  background(0, 0, 0);
}

void draw() {
  while (myPort.available() > 0) {
    myString = myPort.readStringUntil(lf);
    if (myString != null) { 
      num=float(myString);  // Converts and prints float
      println(num);
      background(0, 0, 0);
      fill(255, 100, 0);
      circle(100, 100, num);
      fill(0, 255, 100);
      circle(500, 300, num/2);
      fill(100, 0, 255);
      circle(300, 200, num/3);
    }
  }
  myPort.clear();
} 
