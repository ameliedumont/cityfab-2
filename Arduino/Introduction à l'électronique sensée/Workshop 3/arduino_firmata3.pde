import processing.serial.*;
import cc.arduino.*;

Arduino arduino; //creates arduino object

color back = color(64, 218, 255); //variables for the 2 colors

int sensor= 0;
int photo = 1;
int read;
int read2;

float value;
float ht;


void setup() {
  size(800, 600);
  arduino = new Arduino(this, Arduino.list()[0], 57600); //sets up arduino
    arduino.pinMode(sensor, Arduino.INPUT);//setup pins to be input (A0 =0?)
    arduino.pinMode(photo, Arduino.INPUT);
    //background(back);
}

void draw() {

  //background(back);
  read=arduino.analogRead(sensor);
  read2=arduino.analogRead(photo);
  //background(back);
  println (read);
  value=map(read, 0, 680, 0, width); //use to callibrate
  ht = map(read2, 0, 600, 0, height);
  ellipse(value, ht, 5, 5);
   

}
