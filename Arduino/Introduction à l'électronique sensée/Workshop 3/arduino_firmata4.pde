import processing.serial.*;
import cc.arduino.*;

Arduino arduino; //creates arduino object

color back = color(64, 218, 255); //variables for the 2 colors

int sensor= 0;
int photo = 1;
int read;
int read2;

float value;
float ht;

int milieu;


void setup() {
  size(800, 600, P3D);
  arduino = new Arduino(this, Arduino.list()[0], 57600); //sets up arduino
    arduino.pinMode(sensor, Arduino.INPUT);//setup pins to be input (A0 =0?)
    arduino.pinMode(photo, Arduino.INPUT);
    noFill();
    background(204);
    camera(0.0, height/2, 100.0, 120.0, 50.0, 50.0, 
       0.0, 1.0, 0.0);
}

void draw() {
  milieu = width/2;
  read=arduino.analogRead(sensor);
  read2=arduino.analogRead(photo);
  println (read);
  value=map(read, 0, 680, 0, width); //use to callibrate
  ht = map(read2, 0, 600, 0, height);
  ellipse(milieu+frameCount, ht, value, value);
  ellipse(milieu-frameCount, ht, value, value);
}
