#include <Servo.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

int led1 = 12;
Servo myServo;
String val;

// WiFi network
const char* ssid     = "Orange-3AE1";
const char* password = "LFMEBED876G";

ESP8266WebServer server ( 80 );



char htmlResponse[3000];

void handleRoot() {

  snprintf ( htmlResponse, 3000,
"<!DOCTYPE html>\
<html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
  </head>\
  <body>\
      <style>\
        body{font-family: Monospace;color: white; background-color: navy;}\
        h1{font-size: 5vw;}\
        button{font-family: Monospace;color: white; background-color: navy;border:2px solid white;font-size: 3vw; margin: 2vw;}\
        button:hover{color: navy; background-color: white;}\
        input{font-family: Monospace;color: white; background-color: navy;border:2px solid white;font-size: 3vw; margin: 2vw;width:30vw;height:20vh}\
      </style>\
        <div class='block' style='width:45vw;height:100vh;display:inline-block;border-right: 2px solid white;margin-left: 2vw;'>\
          <h1>Contrôle des leds</h1>\
          <button type='button' name='ledon' id='ledon' size=2> Led ON\
          <button type='button' name='ledoff' id='ledoff' size=2> Led OFF\
        </div>\
        <div class='block' style='left: 50vw; display:inline-block;top:0vh;height:100vh;position:absolute;'>\
          <h1>Contrôle du moteur</h1>\
          <input type='text' name='servo' id='servo' size=2 autofocus>\
          <div>\
          <br><button id=\"save_button\">Envoyer</button>\
          </div>\
        </div>\
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>\    
    <script>\
      var ledon;\
      var ledoff;\
      var servo;\
      $('#save_button').click(function(e){\
        e.preventDefault();\
        servo = $('#servo').val();\        
        $.get('/save?servo=' + servo, function(data){\
          console.log(data);\
        });\
      });\ 
      $('#ledon').click(function(){\
        ledon = 'ledon';\        
        $.get('/save?ledon=' + ledon, function(data){\
          console.log(data);\
        });\
      });\ 
      $('#ledoff').click(function(){\
        ledoff = 'ledoff';\        
        $.get('/save?ledoff=' + ledoff, function(data){\
          console.log(data);\
        });\
      });\     
    </script>\
  </body>\
</html>"); 

   server.send ( 200, "text/html", htmlResponse );  

}


void handleSave() {
  if (server.arg("ledon")!= ""){
    Serial.println(server.arg("ledon"));
    digitalWrite(led1, HIGH);
  } else {
    digitalWrite(led1, LOW);
  }

  if (server.arg("ledoff")!= ""){
    Serial.println(server.arg("ledoff"));
    digitalWrite(led1, LOW);
  }

    if (server.arg("servo")!= ""){
    Serial.println(server.arg("servo"));
    myServo.write((server.arg("servo")).toInt());
  }

}


void setup() {
  pinMode(led1, OUTPUT);
  myServo.attach(2);

  // Start serial
  Serial.begin(115200);
  delay(10);

  // Connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.on ( "/", handleRoot );
  server.on ("/save", handleSave);

  server.begin();
  Serial.println ( "HTTP server started" );


}

void loop() {
  server.handleClient();
}
