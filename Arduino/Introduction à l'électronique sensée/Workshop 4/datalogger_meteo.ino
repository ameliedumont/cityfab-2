#include <SPI.h>
#include <SD.h>
#include <Wire.h>  
#include <Time.h>
#include "RTClib.h"
#include <DS1307RTC.h>
#include <DHT.h>

#define LOG_INTERVAL  900000 // mills between entries
#define ECHO_TO_SERIAL   1 // echo data to serial port

#define DHTPIN 7     
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define LIGHTPIN A0

DHT dht(DHTPIN, DHTTYPE);
RTC_DS3231 rtc; 
    
const int chipSelect = 10;
float humReading;
float tempReading;
int light;

void setup() {
  Serial.begin(9600);
  pinMode(LIGHTPIN, INPUT);

  //setTime(hour(),minute(),second(),day(),month(),year());
  //RTC.set(now());
  
  dht.begin();
  
  delay(3000); 
  
  while (!Serial) {
  }

  Serial.print("Initializing SD card...");

  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    while (1);
  }
  Serial.println("card initialized.");

if (! rtc.begin()) {
   Serial.println("Couldn't find RTC");
   while (1);
}

 if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    // following line sets the RTC to the date & time this sketch was compiled
    //   rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
   rtc.adjust(DateTime(2020, 04, 11, 00, 48, 30));
  }
}


void loop() {
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  //rtc.adjust(DateTime(2020, 05, 04, 16, 40, 30));
  DateTime now = rtc.now();
 
  delay(LOG_INTERVAL);

  humReading = dht.readHumidity();
  tempReading = dht.readTemperature();
  light = analogRead(A0);
  int lightVal = map(light, 0, 1023, 0, 93);
 
  File logfile = SD.open("meteo.csv", FILE_WRITE);

  if (logfile) {
    logfile.print(now.year(), DEC);
    logfile.print("/");
    logfile.print(now.month(), DEC);
    logfile.print("/");
    logfile.print(now.day(), DEC);
    logfile.print(" ");
    logfile.print(now.hour(), DEC);
    logfile.print(":");
    logfile.print(now.minute(), DEC);
    logfile.print(", ");    
    logfile.print(tempReading);
    logfile.print(", ");    
    logfile.print(humReading);
    logfile.print(", ");
    logfile.print(lightVal);
    logfile.print("\n ");
    logfile.close();
    #if ECHO_TO_SERIAL
      Serial.print(now.year());
      Serial.print("/");
      Serial.print(now.month());
      Serial.print("/");
      Serial.print(now.day());
      Serial.print(" ");
      Serial.print(now.hour());
      Serial.print(":");
      Serial.print(now.minute());
      Serial.print(", ");   
      Serial.print(tempReading);
      Serial.print(", ");    
      Serial.println(humReading);
      Serial.print(", ");    
      Serial.println(lightVal);
    #endif //ECHO_TO_SERIAL
  } else {
      Serial.println("error opening logfile.csv");
  }
}
