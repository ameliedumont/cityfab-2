#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#include <DHT.h>

#define DHTPIN 12    
#define DHTTYPE DHT22 

int lightPin = A0;

// WiFi network
const char* ssid     = "*********";
const char* password = "*********";

ESP8266WebServer server ( 80 );
DHT dht(DHTPIN, DHTTYPE);


char htmlResponse[3000];

void handleRoot() {

  snprintf ( htmlResponse, 3000,
"<!DOCTYPE html>\
    <html>\
    <head>\
     <meta charset='UTF-8'>\
    <\head>\
    <style>\
    body {\
      margin: 0;\
    }\
    .card{\
         background: white;\
         padding: 0px;\
         color: navy;\
         box-shadow: 0px 2px 18px -4px rgba(0,0,0,0.75);\
         font-family: monospace;\
         font-size: 5vw;\
         width: 100vw;\
    }\
    .card > div {\
      width: 100vw !important;\
      border-top: 1px solid navy;\
      margin: 0;\
      display:flex;\
      flex-direction: row;\
    }\
    .card > div > p {\
      padding: 2vw;\
      text-align: left;\
      border-right: 1px solid navy;\
      margin: 0;\
      background-color:white;\
      width: auto;\
    }\
    h1{\
      font-size: 5vw;\
      padding: 2vw;\
      text-align: left;\
      font-family: monospace;\
      margin: 0;\
      color: navy;\
    }\
    .titre{\
      border-right: 1px dashed navy;\
      width:49vw;\
      color:mediumseagreen;\
    }\
    </style>\
    <body>\ 
    <h1>Météo Arduino</h1>\
    <div class='card'>\
      <div id='t'>\
        <p class='titre'>Température (°C)</p>\
        <p id='temp'>0</p>\
      </div>\
      <div id='h'>\
        <p class='titre'>Humidité (pourcent)</p>\
        <p id='hum'>0</p>\
      </div>\
      <div id='l'>\
        <p class='titre'>Luminosité (pourcent)</p>\
        <p id='lum'>0</p>\
      </div>\
    </div>\
    <script>\
    let t = document.getElementById('t');\
    let h = document.getElementById('h');\
    let l = document.getElementById('l');\
    setInterval(function() {\
      getTemp();\
      getHum();\
      getLight();\
    }, 5000); \
    function getTemp() {\
      var xhttp = new XMLHttpRequest();\
      xhttp.onreadystatechange = function() {\
        if (this.readyState == 4 && this.status == 200) {\
          document.getElementById('temp').innerHTML = this.responseText;\
          if(Number(this.responseText) <= 17){\
            t.style.backgroundColor = 'azure';\
          } else if(Number(this.responseText) > 17 && Number(this.responseText) <= 24){\
            t.style.backgroundColor = 'lightgreen';\
          } else{\
            t.style.backgroundColor = 'tomato';\
          };\
        };\
      };\
      xhttp.open('GET', 'temperature', true);\
      xhttp.send();\
    };\
    function getHum() {\
      var xhttp = new XMLHttpRequest();\
      xhttp.onreadystatechange = function() {\
        if (this.readyState == 4 && this.status == 200) {\
          document.getElementById('hum').innerHTML = this.responseText;\
          if(Number(this.responseText) > 60) {\
            h.style.backgroundColor = 'aquamarine';\
          } else {\
            h.style.backgroundColor = 'khaki';\
          }\
        };\
      };\
      xhttp.open('GET', 'humidite', true);\
      xhttp.send();\
    };\
    function getLight() {\
      var xhttp = new XMLHttpRequest();\
      xhttp.onreadystatechange = function() {\
        if (this.readyState == 4 && this.status == 200) {\
          document.getElementById('lum').innerHTML = this.responseText;\
        };\
      };\
      xhttp.open('GET', 'luminosite', true);\
      xhttp.send();\
    };\
    </script>\
    </body>\
</html>" ); 

   server.send ( 200, "text/html", htmlResponse );  

}


void sendTemp() {
 int t = dht.readTemperature();
 String tempValue = String(t);
 server.send(200, "text/plane", tempValue); //Send ADC value only to client ajax request
}

void sendHum() {
 int h = dht.readHumidity();
 String humValue = String(h);
 server.send(200, "text/plane", humValue); //Send ADC value only to client ajax request
}

void sendLight() {
 int l = analogRead(lightPin);
 int lv = map(l, 0, 1023, 0, 100);
 String lightValue = String(lv);
 server.send(200, "text/plane", lightValue); //Send ADC value only to client ajax request
}


void setup() {
  pinMode(lightPin, INPUT);
  // Start serial
  Serial.begin(115200);
  delay(10);

  dht.begin();

  // Connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.on ( "/", handleRoot );
  server.on ("/temperature", sendTemp );
  server.on ("/humidite", sendHum );
  server.on ("/luminosite", sendLight );

  server.begin();
  Serial.println ( "HTTP server started" );


}

void loop() {
  server.handleClient();
}
