int temp = A10;
int red = 9;
int green = 6;
int blue = 12;
int val;

void setup() {
  // put your setup code here, to run once:
  pinMode(9, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(12, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  val = analogRead(temp)/4.5;
  Serial.println(val);
  delay(500);

  if(val < 17) {
    displayColor(255, 255, 0);
  }else if(val > 16 && val < 26) {
    displayColor(255, 0, 255);
  } else {
    displayColor(0, 255, 255);
  }
}

void displayColor(int r, int g, int b) {
  analogWrite(red, r);
  analogWrite(green, g);
  analogWrite(blue, b);
}
